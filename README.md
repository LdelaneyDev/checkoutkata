# ShoppingCheckoutApplcation

## Description
This is a console application that allows users to add items to their shopping basket, view the contents of the basket, and apply promotions to their total price at checkout.


## Requirements
This console application requires the following:

.NET 6

NUnit test framework

Moq


## Installation
To install this console application, clone this repository to your local machine using the following command:

'git clone https://gitlab.com/LdelaneyDev/checkoutkata.git'

Once the repository is cloned, navigate to the project directory and run the following command to build the application:

'dotnet build'


## Usage
To use this console application, run the following command in the project directory :

'dotnet run'

This will start the application and display a menu of available shopping items. 

Select an item by entering the corresponding SKU and quantity, and it will be added to your shopping basket. 

You can view the contents of your basket and the total price at any time. 

At checkout, any applicable promotions will be applied to your total price.


## Testing
To run the unit tests for this console application, navigate to the test directory and run the following command:

'dotnet test'

This will run the NUnit tests for the classes in the application and display the results.


## License
This project is licensed under the MIT License. See the LICENSE file for details.
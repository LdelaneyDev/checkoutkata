﻿using System.Collections.Generic;
using CheckoutDomain.PromotionHandler;
using CheckoutDomain.Promotions;
using CheckoutDomain.ShoppingItems;
using Moq;
using NUnit.Framework;

[TestFixture]
public class PromotionHandlerTests
{
    private Mock<IPromotion> _mockPromotion;
    private PromotionHandler _promotionHandler;

    [SetUp]
    public void SetUp()
    {
        // Initialize mock promotion object
        _mockPromotion = new Mock<IPromotion>();
        _mockPromotion.Setup(p => p.Id).Returns(1);
        _mockPromotion.Setup(p => p.ApplyPromotion(It.IsAny<IShoppingItem>(), It.IsAny<int>())).Returns(1.0m);

        // Initialize promotion handler with the mock promotion
        _promotionHandler = new PromotionHandler(new List<IPromotion> { _mockPromotion.Object });
    }

    [Test]
    public void GetTotalPriceWithPromotionApplied_NoPromotions_ReturnsTotalPrice()
    {
        // Arrange
        var itemList = new Dictionary<IShoppingItem, int>
        {
            { new ShoppingItem('A', 1.0m, null), 2 },
            { new ShoppingItem('B', 2.0m, null), 3 },
            { new ShoppingItem('C', 3.0m, null), 4 },
        };

        // Act
        var result = _promotionHandler.GetTotalPriceWithPromotionApplied(itemList);

        // Assert
        Assert.That(result, Is.EqualTo(20.0M));
    }

    [Test]
    public void GetTotalPriceWithPromotionApplied_PromotionApplied_ReturnsAdjustedTotalPrice()
    {
        // Arrange
        var itemList = new Dictionary<IShoppingItem, int>
        {
            { new ShoppingItem('A', 1.0m, 1), 2 },
            { new ShoppingItem('B', 2.0m, null), 3 },
            { new ShoppingItem('C', 3.0m, null), 4 },
        };

        // Act
        var result = _promotionHandler.GetTotalPriceWithPromotionApplied(itemList);

        // Assert
        Assert.AreEqual(19.0m, result);
        _mockPromotion.Verify(p => p.ApplyPromotion(It.IsAny<IShoppingItem>(), It.IsAny<int>()), Times.Exactly(1));
    }
}
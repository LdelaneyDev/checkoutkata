using CheckoutDomain.ShoppingItems;
using Moq;
using Assert = NUnit.Framework.Assert;
namespace CheckoutTests
{
    [TestFixture]
    public class ShoppingItemTests
    {
        private Mock<IShoppingItem> _shoppingItems;

        [SetUp]
        public void Setup()
        {
            _shoppingItems = new Mock<IShoppingItem>();
        }

        [Test]
        public void PromotionToApply_Can_Be_Set_And_Retrieved()
        {
            // Arrange
            _shoppingItems.SetupProperty(x => x.PromotionToApply);

            // Act
            _shoppingItems.Object.PromotionToApply = 1;
            var result = _shoppingItems.Object.PromotionToApply;

            // Assert
            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void ItemSKU_Can_Be_Set_And_Retrieved()
        {
            // Arrange
            _shoppingItems.SetupProperty(x => x.ItemSKU);

            // Act
            _shoppingItems.Object.ItemSKU = 'A';
            var result = _shoppingItems.Object.ItemSKU;

            // Assert
            Assert.That(result, Is.EqualTo('A'));
        }

        [Test]
        public void UnitPrice_Can_Be_Set_And_Retrieved()
        {
            // Arrange
            _shoppingItems.SetupProperty(x => x.UnitPrice);

            // Act
            _shoppingItems.Object.UnitPrice = 2.50m;
            var result = _shoppingItems.Object.UnitPrice;

            // Assert
            Assert.That(result, Is.EqualTo(2.50m));
        }

        [Test]
        public void TestShoppingItemProperties()
        {
            // Arrange
            char itemSKU = 'A';
            decimal unitPrice = 50.00m;
            int? promotionAvailable = 2;

            // Act
            ShoppingItem shoppingItem = new ShoppingItem(itemSKU, unitPrice, promotionAvailable);

            // Assert
            Assert.That(shoppingItem.ItemSKU, Is.EqualTo(itemSKU));
            Assert.That(shoppingItem.UnitPrice, Is.EqualTo(unitPrice));
            Assert.That(shoppingItem.PromotionToApply, Is.EqualTo(promotionAvailable));
        }

        [Test]
        public void TestShoppingItemConstructor()
        {
            // Arrange
            char itemSKU = 'B';
            decimal unitPrice = 30.00m;
            int? promotionAvailable = null;

            // Act
            ShoppingItem shoppingItem = new ShoppingItem(itemSKU, unitPrice, promotionAvailable);

            // Assert
            Assert.IsNotNull(shoppingItem);
            Assert.That(shoppingItem.ItemSKU, Is.EqualTo(itemSKU));
            Assert.That(shoppingItem.UnitPrice, Is.EqualTo(unitPrice));
            Assert.That(shoppingItem.PromotionToApply, Is.EqualTo(promotionAvailable));
        }
    }
}
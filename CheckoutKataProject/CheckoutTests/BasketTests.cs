﻿using CheckoutDomain.Basket;
using CheckoutDomain.ShoppingItems;
using Moq;
using Assert = NUnit.Framework.Assert;
namespace CheckoutTests
{
    [TestFixture]
    public class BasketTests
    {
        private Mock<IShoppingItem> _item;
        private Basket _basket;

        [SetUp]
        public void SetUp()
        {
            _item = new Mock<IShoppingItem>();
            _item.SetupGet(x => x.UnitPrice).Returns(10.00M);
            _basket = new Basket();
            _basket.ItemList = new Dictionary<IShoppingItem, int>();
        }

        [Test]
        public void AddItem_NewItem_AddsItemToList()
        {
            // Arrange
            var newItem = _item.Object;

            // Act
            _basket.AddItem(newItem, 1);

            // Assert
            Assert.That(_basket.ItemList.Count, Is.EqualTo(1));
            Assert.That(_basket.ItemList[newItem], Is.EqualTo(1));
        }

        [Test]
        public void AddItem_ExistingItem_IncrementsQuantity()
        {
            // Arrange
            var existingItem = _item.Object;
            _basket.ItemList.Add(existingItem, 1);

            // Act
            _basket.AddItem(existingItem, 1);

            // Assert
            Assert.That(_basket.ItemList.Count, Is.EqualTo(1));
            Assert.That(_basket.ItemList[existingItem], Is.EqualTo(2));
        }

        [Test]
        public void AddItem_ExistingItem_QuantityUnchanged()
        {
            // Arrange
            var existingItem = _item.Object;
            _basket.ItemList.Add(existingItem, 1);

            // Act
            _basket.AddItem(existingItem, 0);

            // Assert
            Assert.That(_basket.ItemList.Count, Is.EqualTo(1));
            Assert.That(_basket.ItemList[existingItem], Is.EqualTo(1));
        }

        [Test]
        public void AddItem_AddsToTotalPrice()
        {
            // Arrange
            var newItem = _item.Object;

            // Act
            _basket.AddItem(newItem, 2);

            // Assert
            Assert.That(_basket.TotalPrice, Is.EqualTo(20.00M));
        }
    }
}
﻿using CheckoutDomain;
using CheckoutDomain.Basket;
using CheckoutDomain.PromotionHandler;
using CheckoutDomain.Promotions;
using CheckoutDomain.ShoppingItems;

internal class Program
{
    private static void Main(string[] args)
    {
        var continueShopping = true;
        Basket basket = new();

        List<IShoppingItem> availableItems = new List<IShoppingItem>
        {
            new ShoppingItem('A', 10.00M, null),
            new ShoppingItem('B', 15.00M, (int)PromotionEnums._3For40),
            new ShoppingItem('C', 40.00M, null),
            new ShoppingItem('D', 55.00M, (int)PromotionEnums._DiscountForQty2),
        };

        List<IPromotion> activePromotions = new List<IPromotion>
        {
            new Promotion3For40(),
            new PromotionDiscountForSetsOfTwo()
        };

        PromotionHandler promotionHandler = new PromotionHandler(activePromotions);

        while (continueShopping)
        {
            DisplayItemGrid(availableItems, activePromotions);
            DisplayBasketContents(basket);
            DisplayBasketPrice(basket);

            char itemSelected = GetUserItemSelection(availableItems);
            int quantitySelected = GetUserItemQuantitySelection();
            var retrievedItem = GetItemFromList(availableItems, itemSelected);
            basket.AddItem(retrievedItem, quantitySelected);
            Console.WriteLine("Item has been successfully added to basket.");

            continueShopping = RetrieveAndValidateContinueShopping();
            Console.Clear();
        }

        Console.Clear();
        Console.WriteLine("Checkout in process...");
        Thread.Sleep(400);
        Console.WriteLine("Applying Discounts...");
        Thread.Sleep(400);
        basket.FinalPrice = promotionHandler.GetTotalPriceWithPromotionApplied(basket.ItemList);

        if (basket.TotalPrice != basket.FinalPrice)
        {
            var totalPrice = FormatCashValue(basket.TotalPrice);
            Console.WriteLine($"Your basket value before applying promotions: {totalPrice}");

            var finalPrice = FormatCashValue(basket.FinalPrice);
            Console.WriteLine($"Your basket value after applying promotions:  {finalPrice}\n");

            var discountAmount = FormatCashValue(basket.TotalPrice - basket.FinalPrice);
            Console.WriteLine($"You have saved {discountAmount}!\n ");
        }
        else
        {
            Console.WriteLine("No applicable promotions were found.\n");
            DisplayBasketPrice(basket);
        }

        DisplayBasketContents(basket);
        Console.Write("\nPress any key to finish shopping...");
        Console.ReadKey();
    }

    private static string FormatCashValue(decimal cashValue)
    {
        return string.Format("{0:C}", Convert.ToDecimal(cashValue));
    }

    private static char GetUserItemSelection(List<IShoppingItem> availableItems)
    {
        Console.Write($"\nPlease select an Item SKU to add to your basket: ");
        return RetrieveAndValidateItemSelected(availableItems);
    }
    private static int GetUserItemQuantitySelection()
    {
        Console.Write($"Please enter the quantity to add to your basket: ");
        return RetrieveAndValidateQuantity();
    }

    private static void DisplayBasketPrice(Basket basket)
    {
        var totalPrice = FormatCashValue(basket.TotalPrice);
        Console.WriteLine($"Basket Total Price: {totalPrice}");

    }

    private static void DisplayBasketContents(Basket basket)
    {
        Console.WriteLine($"Basket Contents: ");
        if (basket.ItemList.Count == 0)
        {
            Thread.Sleep(100);
            Console.WriteLine("--------------------------");
            Thread.Sleep(100);
            Console.WriteLine($"Your basket is currently empty");
            Thread.Sleep(100);
            Console.WriteLine("--------------------------");
            return;
        }

        foreach (KeyValuePair<IShoppingItem, int> item in basket.ItemList)
        {
            Thread.Sleep(100);
            Console.WriteLine("--------------------------");
            Thread.Sleep(100);
            Console.WriteLine($"SKU: {item.Key.ItemSKU}  -  Quanty: {item.Value}");
            Thread.Sleep(100);
        }
        Console.WriteLine("--------------------------");
    }

    private static IShoppingItem GetItemFromList(List<IShoppingItem> items, char itemSelected)
    {
        return items.Where(x => x.ItemSKU == itemSelected).FirstOrDefault();
    }

    private static void DisplayItemGrid(List<IShoppingItem> availableItems, List<IPromotion> activePromotions)
    {
        Console.WriteLine("Product Inventory:");
        foreach (IShoppingItem item in availableItems)
        {
            var promotionMsg = item.PromotionToApply == null ? "N/A" : activePromotions.Where(x => x.Id == item.PromotionToApply).Select(y => y.Name.ToString()).FirstOrDefault();
            Console.WriteLine($"Item SKU: {item.ItemSKU} - Unit Price: £{item.UnitPrice} - Promotion: {promotionMsg} ");
        }

        Console.Write("*Discounts are applied at checkout.\n\n");
    }

    private static int RetrieveAndValidateQuantity()
    {
        var isValid = false;
        var msg = "Please enter a postive integer value: ";
        var quantity = 0;
        while (!isValid)
        {
            string? input = Console.ReadLine();

            if (string.IsNullOrEmpty(input) || !int.TryParse(input, out int output))
            {
                Console.Write(msg);
                continue;
            }

            if (output <= 0)
            {
                Console.Write(msg);
                continue;
            }

            quantity = output;
            return quantity;
        }

        return quantity;
    }

    private static char RetrieveAndValidateItemSelected(List<IShoppingItem> availableItems)
    {
        var isValid = false;
        var msg = "Please select a valid Item SKU from the inventory grid: ";
        var input = "";
        while (!isValid )
        {
            input = Console.ReadLine().ToUpper();

            if (string.IsNullOrEmpty(input) || input.Length > 1)
            {
                Console.Write(msg);
                continue;
            }

            if (availableItems.Any(x => x.ItemSKU == input[0]))
            { 
                isValid = true;
                break;
            }

            Console.Write(msg);
        }

        return input[0];
    }

    private static bool RetrieveAndValidateContinueShopping()
    {
        var isValid = false;
        Console.WriteLine();
        var msg = "Would you like you continue shopping? (yes/no): ";
        var input = "";
        while (!isValid)
        {
            Console.Write(msg);
            input = Console.ReadLine().ToUpper();

            if (string.IsNullOrEmpty(input))
            {
                Console.Write(msg);
                continue;
            }

            if (input != "YES" && input != "NO")
            {
                Console.Write(msg);
                continue;
            }

            isValid = true;
        }

        return input == "YES" ? true : false;
    }
}
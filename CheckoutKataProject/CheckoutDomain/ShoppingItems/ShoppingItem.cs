﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutDomain.ShoppingItems
{
    public class ShoppingItem : IShoppingItem
    {
        public char ItemSKU { get; set; }
        public decimal UnitPrice { get; set; }
        public int? PromotionToApply { get; set; }

        public ShoppingItem(char itemSKU, decimal unitPrice, int? promotionAvailable) 
        {
            ItemSKU = itemSKU;
            UnitPrice = unitPrice;
            PromotionToApply = promotionAvailable;
        }
    }
}

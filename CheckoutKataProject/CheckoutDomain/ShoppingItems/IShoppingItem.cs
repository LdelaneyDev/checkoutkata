﻿namespace CheckoutDomain.ShoppingItems
{
    public interface IShoppingItem
    {
        char ItemSKU { get; set; }
        decimal UnitPrice { get; set; }
        int? PromotionToApply { get; set; }
    }
}
﻿using CheckoutDomain.ShoppingItems;

namespace CheckoutDomain.Basket
{
    public interface IBasket
    {
        Dictionary<IShoppingItem, int>? ItemList { get; set; }
        decimal TotalPrice { get; set; }
        public decimal FinalPrice { get; set; }
        void AddItem(IShoppingItem item, int quantity);
    }
}

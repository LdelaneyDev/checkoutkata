﻿using CheckoutDomain.ShoppingItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutDomain.Basket
{
    public class Basket : IBasket
    {
        public Dictionary<IShoppingItem, int>? ItemList { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal FinalPrice { get; set; }

        public void InitaliseItemListDictionary()
        {
            ItemList = new Dictionary<IShoppingItem, int>();
        }

        public Basket() 
        {
            InitaliseItemListDictionary();
            TotalPrice = 0.00M;
        }

        public void AddItem(IShoppingItem newItem, int quantity)
        {
            TotalPrice+= (quantity * newItem.UnitPrice);

            if (!ItemList.TryGetValue(newItem, out int oldQuantity))
            {
                ItemList.Add(newItem, quantity);
                return;
            }

            ItemList[newItem] = oldQuantity + quantity;
        }
    }
}

﻿using CheckoutDomain.Promotions;
using CheckoutDomain.ShoppingItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutDomain.PromotionHandler
{
    public interface IPromotionHandler
    {
        Dictionary<int, IPromotion>? Promotions { get; set; }
        decimal GetTotalPriceWithPromotionApplied(Dictionary<IShoppingItem, int> ItemList);
        decimal GetPromotionAdjustment(IShoppingItem item, int quanity, IPromotion promotion);
    }
}

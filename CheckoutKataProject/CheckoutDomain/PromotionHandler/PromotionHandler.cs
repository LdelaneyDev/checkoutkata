﻿using CheckoutDomain.Promotions;
using CheckoutDomain.ShoppingItems;

namespace CheckoutDomain.PromotionHandler
{
    public class PromotionHandler : IPromotionHandler
    {
        public Dictionary<int, IPromotion>? Promotions { get; set; }
        public void InitalisePromotionDictionary(List<IPromotion> activePromotions)
        {
            Promotions = new Dictionary<int, IPromotion>();

            foreach (var promo in activePromotions)
            {
                if (Promotions.ContainsKey(promo.Id))
                    continue;

                Promotions.Add(promo.Id, promo);
            }
        }

        public PromotionHandler(List<IPromotion> activePromotions)
        {
            InitalisePromotionDictionary(activePromotions);
        }

        public decimal GetTotalPriceWithPromotionApplied(Dictionary<IShoppingItem, int> ItemList)
        {
            decimal newTotalPrice = 0.00M;

            foreach (KeyValuePair<IShoppingItem, int> item in ItemList)
            {
                decimal sumItemPrice = item.Key.UnitPrice * item.Value;

                if (!item.Key.PromotionToApply.HasValue)
                {
                    newTotalPrice += sumItemPrice;
                    continue;
                }

                if (Promotions.Count == 0)
                {
                    newTotalPrice += sumItemPrice;
                    continue;
                }

                if (Promotions.ContainsKey((int)item.Key.PromotionToApply))
                {
                    newTotalPrice += GetPromotionAdjustment(item.Key, item.Value, Promotions[(int)item.Key.PromotionToApply]);
                    continue;
                }
                
                newTotalPrice += sumItemPrice;
            }

            return newTotalPrice;
        }

        public decimal GetPromotionAdjustment(IShoppingItem item, int quanity, IPromotion promotion)
        {
            decimal itemSetValueWithPromotionApplied = promotion.ApplyPromotion(item, quanity);

            return itemSetValueWithPromotionApplied;
        }
    }
}
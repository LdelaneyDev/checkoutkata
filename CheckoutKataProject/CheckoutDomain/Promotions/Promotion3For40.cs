﻿using CheckoutDomain.ShoppingItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutDomain.Promotions
{
    public class Promotion3For40 : IPromotion
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int applicableQuantity { get; set; }
        public decimal offerValue { get; set; }
        public decimal discountToApply { get; set; }

        public Promotion3For40()
        {
            Name = "3 for 40";
            Id = 0;
            applicableQuantity = 3;
            offerValue = 40.00M;
        }

        public decimal ApplyPromotion(IShoppingItem item, int quantity)
        {
            var setsOfDeal = Math.DivRem(quantity, applicableQuantity, out int remainder);
            decimal priceWithPromotion = (setsOfDeal * offerValue) + (item.UnitPrice * remainder);

            return priceWithPromotion;
        }
    }
}

﻿using CheckoutDomain.ShoppingItems;

namespace CheckoutDomain.Promotions
{
    public class PromotionDiscountForSetsOfTwo : IPromotion
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int applicableQuantity { get; set; }
        public decimal offerValue { get; set; }
        public decimal discountToApply { get; set; }

        public PromotionDiscountForSetsOfTwo()
        {
            Name = "25% off for every 2 purchased together";
            Id = 1;
            applicableQuantity = 2;
            discountToApply = 0.25M;
        }

        public decimal ApplyPromotion(IShoppingItem item, int quantity)
        {
            var setsOfDeal = Math.DivRem(quantity, applicableQuantity, out int remainder);
            decimal priceWithPromotion = ((setsOfDeal * (item.UnitPrice * 2)) * (1 - discountToApply)) + (item.UnitPrice * remainder);

            return priceWithPromotion;
        }
    }
}

﻿using CheckoutDomain.ShoppingItems;

namespace CheckoutDomain.Promotions
{
    public interface IPromotion
    {
        string Name { get; set; }
        int Id { get; set; }
        int applicableQuantity { get; set; }
        decimal offerValue { get; set; }
        decimal discountToApply { get; set; }

        decimal ApplyPromotion(IShoppingItem item, int quantity);
    }
}